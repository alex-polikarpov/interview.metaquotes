# Скриншоты личного кабинета от имени пользователя

#### Главная, таблица с заметками

Есть возможность сортировки по каждому столбцу, обычный пользователь видит свои заметки и публичные заметки.
![MetaQuotes4](https://gitlab.com/23Alex24/interview.metaquotes/raw/master/images/MetaQuotes4.png)

#### Главная, таблица с заметками
В табличке постраничная пагинация

![MetaQuotes5](https://gitlab.com/23Alex24/interview.metaquotes/raw/master/images/MetaQuotes5.png)

#### Фильтрация по ключевой фразе
Поиск ищет совпадение в теме и в содержимом заметки

![MetaQuotes6](https://gitlab.com/23Alex24/interview.metaquotes/raw/master/images/MetaQuotes6.png)

#### Просмотр публичной заметки
Текущий пользователь НЕ является администраторм, поэтому при просмотре публичных заметок он не может их редактировать.

![MetaQuotes7](https://gitlab.com/23Alex24/interview.metaquotes/raw/master/images/MetaQuotes7.png)

#### Просмотр собственной заметки
Собственные заметки пользователь может редактировать.

![MetaQuotes8](https://gitlab.com/23Alex24/interview.metaquotes/raw/master/images/MetaQuotes8.png)

#### Редактирование своей заметки
Поля обязательны для заполнения

![MetaQuotes9](https://gitlab.com/23Alex24/interview.metaquotes/raw/master/images/MetaQuotes9.png)

Стоит ограничение на максимальную длину на полях ввода

![MetaQuotes10](https://gitlab.com/23Alex24/interview.metaquotes/raw/master/images/MetaQuotes10.png)

#### Сохранение заметки 
После сохранения показываем плашку. При сохранении остаемся на текущей странице, можно было бы делать редирект на главную, 
но т.к. параметры фильтрации и сортировки не передаются через url и не сохраняются, то было бы неудобно.

![MetaQuotes11](https://gitlab.com/23Alex24/interview.metaquotes/raw/master/images/MetaQuotes11.png)
