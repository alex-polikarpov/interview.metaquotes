# Скриншоты

* [Скриншоты страницы входа](https://gitlab.com/23Alex24/interview.metaquotes/blob/master/docs/screenshots_signin.md)
* [Скриншоты личного кабинета от имени пользователя, фукнционал редактирования заметок](https://gitlab.com/23Alex24/interview.metaquotes/blob/master/docs/screenshots_user.md)
* [Скриншоты личного кабинета от имени администратора](https://gitlab.com/23Alex24/interview.metaquotes/blob/master/docs/screenshots_admin.md)