# Техническая документация

* [Архитектура](https://gitlab.com/23Alex24/interview.metaquotes/blob/master/docs/technical/technical_docs_architecture.md)
* [Front-end](https://gitlab.com/23Alex24/interview.metaquotes/blob/master/docs/technical/technical_docs_frontend.md)
* [Сбор статистики и парсинг логов](https://gitlab.com/23Alex24/interview.metaquotes/blob/master/docs/technical/technical_docs_logs.md)

